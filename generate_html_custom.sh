# set -ex

apt update
apt-get install jq -y

# Create Deliverable List (URL documents)
python3 makeDelList/main.py
mv makeDelList/deliverables_list.md docs

# Wordpress
content=$(curl -u "$WPAPPUSER:$WPAPPPASS" -X GET https://members.gaia-x.eu/index.php/wp-json/wp/v2/pages/25 | jq ".content.rendered")

# Remove " at the beginning and end of the file, \r, \n and \
data=$(echo ${content:1:-1} | sed 's/\\r/ /g' | sed 's/\\n/ /g' | sed 's/\\/ /g')

modified=${data%<p><\!--Deliverables_list--\!></p>*}
modified+="<p><!--Deliverables_list--!></p>"
links=$(cat makeDelList/deliverables_links.txt)
modified+=$links

curl --user "$WPAPPUSER:$WPAPPPASS" -X POST https://members.gaia-x.eu/index.php/wp-json/wp/v2/pages/25 -d "content=$modified" | jq .

# Create md files from yaml files
bash docs/mission_documents/build_md.sh

# Merge technical committee WG files under the technical committee one
tc_files=docs/mission_documents/technical-committee_*.md
tc=docs/mission_documents/technical-committee.md

# Create new Technical Committe md where we merge all other tc md, including th tc one. The goal is to have all WG and the TC specs cleanly displayed in the navigation bar
echo "# Technical Committee" > docs/mission_documents/Technical_Committee.md

cat $tc >> docs/mission_documents/Technical_Committee.md

for file in ${tc_files[@]}; do
  echo $file
  cat $file >> docs/mission_documents/Technical_Committee.md
done

# Move all remaining md files into docs
mv docs/mission_documents/*.md docs

# Remove all tc wg md files and tc md from docs
rm docs/technical-committee.md
md_files=docs/technical-committee_*.md
for file in ${md_files[@]}; do
  rm $file
done


